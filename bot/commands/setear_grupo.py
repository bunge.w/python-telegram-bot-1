from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters

from bot.constants import CONVERSATIONS
from bot.db import get_msg_guita
from bot.utils import restricted
from .cancel import cancel

def create_handler():
    # https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py
    # Add conversation handler
    return ConversationHandler(
        entry_points=[CommandHandler('setear_grupo', start_command)],
        states={
            CONVERSATIONS.GROUP: [MessageHandler((Filters.text & ~Filters.command), command)],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

@restricted
def start_command(update, context):
    update.message.reply_text(f'⏺ Okey, mandame el nombre del nuevo grupo (o cancelá con /cancelar):')
    return CONVERSATIONS.GROUP

@restricted
def command(update, context):
    group = update.message.text

    # si es un int es un id directo y lo dejamos sin arroba
    # si no es int asumimos que es un groupname y verificamos que tenga arroba
    try:
        group = str(int(group))
    except ValueError:
        if group[0] != '@':
            group = f'@{group}'

    # updateamos DB
    msg = get_msg_guita()
    msg.grupo = group
    msg.save()

    # devolvemos
    update.message.reply_text(f'✅ Grupo seteado con éxito')
    return ConversationHandler.END
