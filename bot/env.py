import sys, os

def try_get(KEY):
    value=os.getenv(KEY)
    if not value:
        print('Falta proporcionar:', KEY)
        sys.exit(1)
    return value

def try_get_multiple(KEYS):
    values=[]
    missing=[]
    if type(KEYS) == str:
        KEYS = [k.strip() for k in KEYS.split(',')]
    for KEY in KEYS:
        value = os.getenv(KEY)
        if not value:
            missing.append(KEY)
        else:
            values.append(value)
    if missing:
        print('Falta proporcionar:', ', '.join(missing))
        sys.exit(1)
    return values
