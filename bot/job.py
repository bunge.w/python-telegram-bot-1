import datetime

from peewee import *
from pytz import timezone

from bot.db import get_msg_guita

# guía jobs - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-JobQueue
# refe job_queue - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.jobqueue.html
def job_action(context):
    msg = get_msg_guita()
    if msg.activo:
        texto_i = 0 if msg.last_sent_texto >= len(msg.textos)-1 else msg.last_sent_texto+1
        texto = msg.textos[texto_i]

        context.bot.send_message(chat_id=msg.grupo, text=texto.contenido, parse_mode='MarkdownV2')

        msg.last_sent_texto = texto_i
        msg.save()

def get_msg_job(context):
    msg = get_msg_guita()
    jobs = context.job_queue.get_jobs_by_name(msg.id)
    return jobs[0] if len(jobs) else None

def add_msg_job(context):
    msg = get_msg_guita()

    if msg.activo:
        # dias es 7 letras "1" o "0" según encendido apagado de domingo-a-sábado
        msg_days_split = [d for d in msg.dias]
        days = []
        for i, day in enumerate(msg_days_split):
            if day == '1':
                days.append(i)

        if days:
            # el tzinfo no se guarda en la base de datos y por eso hay que recontruir acá
            # America/Araguaina = GMT-3
            time = datetime.time(msg.horario.hour, msg.horario.minute, msg.horario.second, tzinfo=timezone('America/Araguaina'))

            # run_daily - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.jobqueue.html#telegram.ext.JobQueue.run_daily
            context.job_queue.run_daily(job_action, time=time, days=days, name=msg.id)

def clean_msg_job(context):
    msg_job = get_msg_job(context)
    if msg_job:
        msg_job.schedule_removal()

def restart_msg_job(context):
    clean_msg_job(context)
    msg = get_msg_guita()
    if msg.activo:
        add_msg_job(context)
